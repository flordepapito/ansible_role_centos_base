import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_docker_is_working(host):
    cmd = host.run("sudo docker run centos:7 /usr/bin/ping -c1 google.com")

    assert cmd.rc == 0
